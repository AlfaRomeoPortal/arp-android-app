package de.alfa.romeo.portal.asyntasks;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import org.json.JSONException;

import java.util.Map;

import de.alfa.romeo.portal.app.MessageList;
import de.alfa.romeo.portal.app.R;
import de.alfa.romeo.portal.communication.ArpConnector;
import de.alfa.romeo.portal.couchbase.CouchbaseHelper;
import de.alfa.romeo.portal.models.LoginResponseObject;
import de.alfa.romeo.portal.models.message.MessageListItem;
import de.alfa.romeo.portal.models.message.MessageListResponseObject;

/**
 * Created by andre on 10.06.14.
 *
 * Dieser Service liest nicht nur wiederholend PMs aus, sondern sorgt auch dafür
 * dass der User Login regelmäßig erneuert wird
 *
 */
public class ServiceMessagesTask extends AsyncTask<Context ,Integer,MessageListResponseObject> {

    private Context context;
    private Map<String, Object> userPreferences;

    @Override
    protected MessageListResponseObject doInBackground(Context... params) {

        this.context = params[0];

        MessageListResponseObject messageListResponseObject = null;

        // get couchbase document with user preferences
        CouchbaseHelper couchbaseHelper = new CouchbaseHelper(params[0].getApplicationContext());
        this.userPreferences = couchbaseHelper.queryUserPreferences();
        couchbaseHelper.closeDb();

        if(this.userPreferences != null) {
            // get api connector
            ArpConnector arpConnector = new ArpConnector();

            try {
                LoginResponseObject respLogin = null;

                // api init
                Map<String,Object> inputParams = arpConnector.apiInit();

                // api login
                respLogin = arpConnector.login(this.userPreferences.get("login").toString(),
                        this.userPreferences.get("password").toString(), inputParams);


                if(respLogin != null) {
                    if (!respLogin.getUserId().equals("") && !respLogin.getDbSessionHash().equals("") &&
                            respLogin.getAction().equals("redirect_login")) {

                        // get message list from api
                        messageListResponseObject = arpConnector.getMessageList(inputParams);
                        return messageListResponseObject;
                    } else {
                        throw new Exception("could not login user!");
                    }
                } else {
                    throw new Exception("login response is null!");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        return messageListResponseObject;
    }


    @Override
    protected void onPostExecute(MessageListResponseObject messageListResponseObject) {
        if(messageListResponseObject != null){
            int i = 1;
            String text = "";
            boolean foundNewMessage = false;

            for(MessageListItem message : messageListResponseObject.getMessages()){
                // check if there are new messages
                if(message.getStatusIcon().equals("new")){

                    foundNewMessage = true;

                    // update text of notification
                    if(i == 1) {
                        text = "Neue PM von " + message.getSenderUserName();
                    } else {
                        text = "Du hast " + i + " neue Nachrichten.";
                    }
                    i++;
                }
            }


            if(foundNewMessage){
                // create notification
                NotificationManager notificationManager =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                Intent messageListIntent = new Intent(context, MessageList.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, messageListIntent, 0);

                Notification notification = new Notification.Builder(context)
                        .setSmallIcon(R.drawable.alfalogo)
                        .setLargeIcon(BitmapFactory.decodeResource(this.context.getResources(), R.drawable.alfalogo))
                        .setContentTitle(this.context.getResources().getString(R.string.service_new_message))
                        .setContentText(text)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                                //.addAction(R.drawable.mailnew_white, "Öffnen", pendingIntent).setAutoCancel(true)
                        //.build(); // needed ab SDK 16
                        .getNotification(); // needed to support SDK 14 and 15

                notificationManager.notify(1, notification);
            }
        }
    }
}
