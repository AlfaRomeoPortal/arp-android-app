package de.alfa.romeo.portal.asyntasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.app.R;
import de.alfa.romeo.portal.communication.ArpConnector;
import de.alfa.romeo.portal.models.LoginResponseObject;
import de.alfa.romeo.portal.models.message.MessageResponseObject;

/**
 * Created by andre on 01.08.14.
 */
public class GetMessageTask extends AsyncTask<List<String>,Integer, MessageResponseObject>{

    private Context context;
    private Activity activity;

    public GetMessageTask(Context context, Activity activity){
        this.context = context;
        this.activity = activity;
    }

    @Override
    protected MessageResponseObject doInBackground(List<String>... params) {

        ProgressBar progressBar = (ProgressBar)this.activity.findViewById(R.id.pbSpinner);
        progressBar.setVisibility(View.VISIBLE);

        List<String> parameters = params[0];

        ArpConnector arpConnector = new ArpConnector();
        MessageResponseObject messageResponseObject = null;
        LoginResponseObject respLogin = null;

        // api init
        Map<String,Object> inputParams = arpConnector.apiInit();

        // api login
        respLogin = arpConnector.login(parameters.get(0), parameters.get(1), inputParams);


        if(respLogin != null) {
            return messageResponseObject = arpConnector.getPm(parameters.get(2), inputParams);
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(MessageResponseObject messageResponseObject) {
        Toast toast;
        if(messageResponseObject != null) {
            ((TextView) activity.findViewById(R.id.messagetext)).setText(messageResponseObject.getMessage());
            ((TextView) activity.findViewById(R.id.messagetitle)).setText(messageResponseObject.getTitle());
            ((TextView) activity.findViewById(R.id.sender_name)).setText(messageResponseObject.getUserName());
            ((TextView) activity.findViewById(R.id.sender_title)).setText(messageResponseObject.getUserTitle());
            ((TextView) activity.findViewById(R.id.sender_id)).setText(messageResponseObject.getUserId());

            if(messageResponseObject.getOnlineStatus() == 1){
                ((ImageView) activity.findViewById(R.id.onlinestatus)).setVisibility(View.VISIBLE);
            } else {
                ((ImageView) activity.findViewById(R.id.onlinestatus)).setVisibility(View.INVISIBLE);
            }

            try {
                String url = "http://www.alfa-romeo-portal.de/arcommunity/image.php?u=" + messageResponseObject.getUserId();
                new DownloadImageTask((ImageView)activity.findViewById(R.id.avatar)).execute(new URL(url));
            } catch (MalformedURLException e) {}

            toast = Toast.makeText(context, "Nachricht erfolgreich geladen", Toast.LENGTH_SHORT);
        } else {
            toast = Toast.makeText(context, "Nachricht konnte nicht geladen werden", Toast.LENGTH_SHORT);
        }

        toast.show();

        activity.findViewById(R.id.pbSpinner).setVisibility(View.INVISIBLE);
    }
}
