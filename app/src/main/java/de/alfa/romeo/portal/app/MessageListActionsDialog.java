package de.alfa.romeo.portal.app;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


public class MessageListActionsDialog extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list_actions_dialog);

        Bundle intentData = getIntent().getExtras();
        Log.d(BaseActivity.TAG, "got the following message id: " + String.valueOf(intentData.getInt("messageId")));
    }


    public void deleteMessage(View button){
        Log.d(BaseActivity.TAG, "Delete message was clicked...");
        finish();
    }



}
