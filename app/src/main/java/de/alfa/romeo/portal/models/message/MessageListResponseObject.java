package de.alfa.romeo.portal.models.message;

import java.util.List;

/**
 * Created by andre on 14.05.14.
 */
public class MessageListResponseObject {

    private int totalPmsStored;
    private int totalPmsInbox;
    private int showPerPage;
    private int startMessage;
    private int endMessage;
    private int pmQuota;
    private List<MessageListItem> messages;


    public int getTotalPmsStored() {
        return totalPmsStored;
    }

    public void setTotalPmsStored(int totalPmsStored) {
        this.totalPmsStored = totalPmsStored;
    }

    public int getTotalPmsInbox() {
        return totalPmsInbox;
    }

    public void setTotalPmsInbox(int totalPmsInbox) {
        this.totalPmsInbox = totalPmsInbox;
    }

    public int getShowPerPage() {
        return showPerPage;
    }

    public void setShowPerPage(int showPerPage) {
        this.showPerPage = showPerPage;
    }

    public int getStartMessage() {
        return startMessage;
    }

    public void setStartMessage(int startMessage) {
        this.startMessage = startMessage;
    }

    public int getEndMessage() {
        return endMessage;
    }

    public void setEndMessage(int endMessage) {
        this.endMessage = endMessage;
    }

    public int getPmQuota() {
        return pmQuota;
    }

    public void setPmQuota(int pmQuota) {
        this.pmQuota = pmQuota;
    }

    public List<MessageListItem> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageListItem> messages) {
        this.messages = messages;
    }
}
