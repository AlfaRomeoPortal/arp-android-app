package de.alfa.romeo.portal.couchbase;

import android.content.Context;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.Manager;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.UnsavedRevision;
import com.couchbase.lite.View;
import com.couchbase.lite.android.AndroidContext;
import com.couchbase.lite.util.Log;

import java.io.IOException;
import java.util.Map;

import de.alfa.romeo.portal.app.BaseActivity;

/**
 * Created by andre on 06.06.14.
 */
public class CouchbaseHelper {

    private static Manager cbManager;
    public final static String CB_BUCKET_NAME = "arp-bucket";
    public final static String CB_VIEW_API_PREFERENCES = "apipreferences";
    public final static String CB_VIEW_USER_PREFERENCES = "userpreferences";

    private Database cbDatabase;
    private View apiPreferences;
    private View userPreferences;

    public CouchbaseHelper(Context context){
        try {
            // enable logging
            Manager.enableLogging(Log.TAG, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_SYNC, Log.DEBUG);
            Manager.enableLogging(Log.TAG_QUERY, Log.DEBUG);
            Manager.enableLogging(Log.TAG_VIEW, Log.DEBUG);
            Manager.enableLogging(Log.TAG_DATABASE, Log.DEBUG);

            this.cbManager = new Manager(new AndroidContext(context), Manager.DEFAULT_OPTIONS);

            initDb();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void initDb(){
        try {
            // create db if not exists otherwise we get a handle to the existing database
            this.cbDatabase = cbManager.getDatabase(CouchbaseHelper.CB_BUCKET_NAME);

            // create needed views if they don't exist
            this.apiPreferences     = this.cbDatabase.getView(CouchbaseHelper.CB_VIEW_API_PREFERENCES);
            this.userPreferences    = this.cbDatabase.getView(CouchbaseHelper.CB_VIEW_USER_PREFERENCES);


        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }


    /**
     * create a new api preferences document in couchbase or update an existing one
     * @param params
     * @return document id
     */
    public String createDocApiPreferences(final Map<String, Object> params) {
        Document apiPrefDoc = null;
        // check if document already exists
        Map<String, Object> preferences = this.queryApiPreferences();

        if(params != null && preferences == null) {
            // create new document
            apiPrefDoc = this.cbDatabase.createDocument();
            try {

                for(Map.Entry<String,Object> entry : params.entrySet()){
                    android.util.Log.i(BaseActivity.TAG, "CouchbaseHelper (createDoc) found: key=" + entry.getKey() + " value: " + entry.getValue());
                }

                apiPrefDoc.putProperties(params);
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
                return null;
            }

            return apiPrefDoc.getId();

        } else if(preferences != null) {
            // update document
            apiPrefDoc = this.cbDatabase.getDocument((String)preferences.get("_id"));

            try {
                apiPrefDoc.update(new Document.DocumentUpdater() {
                    @Override
                    public boolean update(UnsavedRevision newRevision) {
                        Map<String, Object> properties = newRevision.getUserProperties();

                        properties.put("accesstoken", params.get("accesstoken"));
                        properties.put("sessionHash", params.get("sessionHash"));
                        properties.put("secret", params.get("secret"));
                        properties.put("apiClientId", params.get("apiClientId"));
                        properties.put("login", params.get("login"));
                        properties.put("password", params.get("password"));

                        for(Map.Entry<String,Object> entry : properties.entrySet()){
                            android.util.Log.i(BaseActivity.TAG, "CouchbaseHelper (updateDoc) found: key=" + entry.getKey() + " value: " + entry.getValue());
                        }

                        newRevision.setUserProperties(properties);
                        return true;
                    }
                });

                return apiPrefDoc.getId();
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            // do nothing
            return apiPrefDoc.getId();
        }
    }



    public String createDocUserPreferences(final Map<String, Object> params) {
        Document userPrefDoc = null;
        // check if document already exists
        Map<String, Object> preferences = this.queryUserPreferences();

        if(params != null && preferences == null) {
            // create new document
            userPrefDoc = this.cbDatabase.createDocument();
            try {

                for(Map.Entry<String,Object> entry : params.entrySet()){
                    android.util.Log.i(BaseActivity.TAG, "CouchbaseHelper (createDocUserPreferences) found: key=" + entry.getKey() + " value: " + entry.getValue());
                }

                userPrefDoc.putProperties(params);
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
                return null;
            }

            return userPrefDoc.getId();

        } else if(preferences != null) {
            // update document
            userPrefDoc = this.cbDatabase.getDocument((String)preferences.get("_id"));

            try {
                userPrefDoc.update(new Document.DocumentUpdater() {
                    @Override
                    public boolean update(UnsavedRevision newRevision) {
                        Map<String, Object> properties = newRevision.getUserProperties();

                        properties.put("login", params.get("login"));
                        properties.put("password", params.get("password"));
                        properties.put("enableNotification", params.get("enableNotification"));
                        properties.put("notificationInterval", params.get("notificationInterval"));

                        for(Map.Entry<String,Object> entry : properties.entrySet()){
                            android.util.Log.i(BaseActivity.TAG, "CouchbaseHelper (updateDocUserPreferences) found: key=" + entry.getKey() + " value: " + entry.getValue());
                        }

                        newRevision.setUserProperties(properties);
                        return true;
                    }
                });

                return userPrefDoc.getId();
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            // do nothing
            return userPrefDoc.getId();
        }
    }



    public Map<String, Object> queryApiPreferences(){

        Map<String, Object> preferences = null;

        // set map function before querying - map functions are not persistet in cb lite!
        // if map function changes the version number has to be incremented
        this.apiPreferences.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                // check if document_type = apipreferences
                if(document.get("document_type").equals(CouchbaseHelper.CB_VIEW_API_PREFERENCES)) {
                    emitter.emit(document.get("_id"), document.get("document_type"));
                }
            }
        }, "1");

        Query query = this.apiPreferences.createQuery();

        try {
            // start querying
            QueryEnumerator queryEnumerator = query.run();

            android.util.Log.d(BaseActivity.TAG, "found "  + queryEnumerator.getCount() + " docs for api preferences");

            // check if document found
            if(queryEnumerator.getCount() > 0){
                String documentId = queryEnumerator.getRow(0).getDocumentId();
                Document docApiPreferences = this.cbDatabase.getDocument(documentId);
                preferences = docApiPreferences.getProperties();
            }
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }

        return preferences;
    }


    public Map<String, Object> queryUserPreferences(){

        Map<String, Object> preferences = null;

        // set map function before querying - map functions are not persistet in cb lite!
        // if map function changes the version number has to be incremented
        this.userPreferences.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                // check if document_type = userpreferences
                if(document.get("document_type").equals(CouchbaseHelper.CB_VIEW_USER_PREFERENCES)) {
                    emitter.emit(document.get("_id"), document.get("document_type"));
                }
            }
        }, "1");

        Query query = this.userPreferences.createQuery();

        try {
            // start querying
            QueryEnumerator queryEnumerator = query.run();

            android.util.Log.d(BaseActivity.TAG, "found "  + queryEnumerator.getCount() + " docs for user preferences");

            // check if document found
            if(queryEnumerator.getCount() > 0){
                String documentId = queryEnumerator.getRow(0).getDocumentId();
                Document docUserPreferences = this.cbDatabase.getDocument(documentId);
                preferences = docUserPreferences.getProperties();
            }
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }

        return preferences;
    }


    /**
     * delete given couchbase db
     *
     * @param dbName
     * @return
     */
    public boolean deleteDb(String dbName){
        try {
            if(this.cbDatabase != null) {
                this.cbDatabase.delete();
                this.cbDatabase.close();

                android.util.Log.d(BaseActivity.TAG, "sucessfully deleted bucket...");
                return true;
            }
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean closeDb(){
        this.cbDatabase.close();
        return true;
    }

}
