package de.alfa.romeo.portal.models.buddy;

/**
 * Created by andre on 16.04.14.
 */
public class BuddyObject {
    private Buddy buddy;
    private Show show;


    public BuddyObject() {
    }

    public Buddy getBuddy() {
        return buddy;
    }

    public void setBuddy(Buddy buddy) {
        this.buddy = buddy;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }
}
