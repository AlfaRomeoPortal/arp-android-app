package de.alfa.romeo.portal.models.buddy;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.net.MalformedURLException;
import java.net.URL;

import de.alfa.romeo.portal.adapter.BuddyListAdapter;
import de.alfa.romeo.portal.helper.BitmapDownloader;

/**
 * Created by andre on 16.04.14.
 */
public class Buddy {
    private String username;
    private String invisible;
    private String userId;
    private long lastActivity;
    private String buddymark;
    private String invisibleMark;
    private String online;
    private String onlinestatusphrase;
    private String statusIcon;

    private Bitmap avatarImage;
    private BuddyListAdapter buddyListAdapter;

    public Buddy() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getInvisible() {
        return invisible;
    }

    public void setInvisible(String invisible) {
        this.invisible = invisible;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(long lastActivity) {
        this.lastActivity = lastActivity;
    }

    public String getBuddymark() {
        return buddymark;
    }

    public void setBuddymark(String buddymark) {
        this.buddymark = buddymark;
    }

    public String getInvisibleMark() {
        return invisibleMark;
    }

    public void setInvisibleMark(String invisibleMark) {
        this.invisibleMark = invisibleMark;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getOnlinestatusphrase() {
        return onlinestatusphrase;
    }

    public void setOnlinestatusphrase(String onlinestatusphrase) {
        this.onlinestatusphrase = onlinestatusphrase;
    }

    public String getStatusIcon() {
        return statusIcon;
    }

    public void setStatusIcon(String statusIcon) {
        this.statusIcon = statusIcon;
    }

    public Bitmap getBitmap() {
        return avatarImage;
    }

    public void setBitmap(Bitmap bitmap) {
        this.avatarImage = bitmap;
    }

    public BuddyListAdapter getBuddyListAdapter() {
        return buddyListAdapter;
    }

    public void setBuddyListAdapter(BuddyListAdapter buddyListAdapter) {
        this.buddyListAdapter = buddyListAdapter;
    }


    public void loadAvatar(){
        if(buddyListAdapter != null){
            String url = "http://www.alfa-romeo-portal.de/arcommunity/image.php?u=" + userId;
            try {
                new ImageDownloadTask().execute(new URL(url));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    // Async Task to fetch buddy avatar image
    private class ImageDownloadTask extends AsyncTask<URL, Integer, Bitmap> {

        @Override
        protected Bitmap doInBackground(URL... params) {
            Bitmap bitmap = BitmapDownloader.loadBitmap(params[0]);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            if(bitmap != null && bitmap.getByteCount() > 1) {
                avatarImage = bitmap;

                if(buddyListAdapter != null){
                    buddyListAdapter.notifyDataSetChanged();
                }
            } else {
            }
        }
    }

}
