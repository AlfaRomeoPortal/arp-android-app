package de.alfa.romeo.portal.asyntasks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.adapter.BuddyListAdapter;
import de.alfa.romeo.portal.app.CreateMessage;
import de.alfa.romeo.portal.app.R;
import de.alfa.romeo.portal.communication.ArpConnector;
import de.alfa.romeo.portal.models.LoginResponseObject;
import de.alfa.romeo.portal.models.buddy.BuddyListResponseObject;
import de.alfa.romeo.portal.models.buddy.BuddyObject;

/**
 * Created by andre on 01.08.14.
 */
public class GetBuddyListTask extends AsyncTask<List<String>,Integer, BuddyListResponseObject>{

    private Context context;
    private Activity activity;

    public GetBuddyListTask(Context context, Activity activity){
        this.context = context;
        this.activity = activity;
    }

    @Override
    protected BuddyListResponseObject doInBackground(List<String>... params) {

        List<String> userLogin = params[0];

        ArpConnector arpConnector = new ArpConnector();
        BuddyListResponseObject respBuddyList = null;
        LoginResponseObject respLogin = null;

        // api init
        Map<String,Object> inputParams = arpConnector.apiInit();

        // apii login
        respLogin = arpConnector.login(userLogin.get(0), userLogin.get(1), inputParams);

        // api get buddylist
        respBuddyList = arpConnector.getBuddyList(inputParams);
        return respBuddyList;
    }

    @Override
    protected void onPostExecute(BuddyListResponseObject respBuddyList) {
        ListView lvBuddies = (ListView)this.activity.findViewById(R.id.lvBuddies);
        SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout)activity.findViewById(R.id.content);

        if(respBuddyList != null && respBuddyList.getResponse() != null) {

            if(respBuddyList.getResponse().getOnlineUsers().size() == 0) {
                Toast toast = Toast.makeText(context, "Keiner deiner Freunde ist online.", Toast.LENGTH_SHORT);
                toast.show();
                return;
            }

            final BuddyListAdapter buddyListAdapter = new BuddyListAdapter(respBuddyList, this.activity);
            lvBuddies.setAdapter(buddyListAdapter);

            // load async image for every buddy
            for(BuddyObject buddy : respBuddyList.getResponse().getOnlineUsers()){
                buddy.getBuddy().setBuddyListAdapter(buddyListAdapter);
                buddy.getBuddy().loadAvatar();
            }

            lvBuddies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent newPM = new Intent(context, CreateMessage.class);
                    newPM.putExtra("senderId", ((BuddyObject)buddyListAdapter.getItem(position)).getBuddy().getUserId());
                    newPM.putExtra("senderName", ((BuddyObject)buddyListAdapter.getItem(position)).getBuddy().getUsername());
                    newPM.putExtra("senderTitle", ((BuddyObject)buddyListAdapter.getItem(position)).getBuddy().getOnline());
                    newPM.putExtra("title", "");

                    if(((BuddyObject)buddyListAdapter.getItem(position)).getBuddy().getOnline().equals("online")) {
                        newPM.putExtra("onlineStatus", 0);
                    } else {
                        newPM.putExtra("onlineStatus", 4);
                    }

                    newPM.putExtra("messageId", "");

                    activity.startActivity(newPM);
                }
            });

            Toast toast = Toast.makeText(context, "Deine Freunde, die online sind wurden geladen.", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            Toast toast = Toast.makeText(context, "Es gab Probleme beim Laden deiner Freundesliste.", Toast.LENGTH_SHORT);
            toast.show();
            activity.finish();
        }

        // stop refresh animation
        swipeLayout.setRefreshing(false);
    }
}
