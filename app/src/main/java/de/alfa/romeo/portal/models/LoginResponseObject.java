package de.alfa.romeo.portal.models;

/**
 * Created by andre on 11.04.14.
 */
public class LoginResponseObject {

    private String dbSessionHash;
    private String userId;
    private String userName;
    private String action;

    public LoginResponseObject(){

    }

    public String getDbSessionHash() {
        return dbSessionHash;
    }

    public void setDbSessionHash(String dbSessionHash) {
        this.dbSessionHash = dbSessionHash;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
