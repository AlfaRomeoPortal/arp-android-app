package de.alfa.romeo.portal.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import de.alfa.romeo.portal.app.R;
import de.alfa.romeo.portal.models.album.AlbumListResponseObject;

/**
 * Created by andre on 23.08.14.
 */
public class AlbumListAdapter extends BaseAdapter {

    private Activity activity;
    private AlbumListResponseObject albumListResponseObject;

    public AlbumListAdapter(Activity activity, AlbumListResponseObject albumListResponseObject) {
        this.activity = activity;
        this.albumListResponseObject = albumListResponseObject;
    }

    @Override
    public int getCount() {
        return this.albumListResponseObject.getAlbumList().size();
    }

    @Override
    public Object getItem(int position) {
        return this.albumListResponseObject.getAlbumList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            LayoutInflater inflater =
                    (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.album_overview_item, parent,false);
        }

        TextView albumTitel = (TextView)convertView.findViewById(R.id.albumTitel);
        albumTitel.setText(this.albumListResponseObject.getAlbumList().get(position).getTitle());

        ImageView cover = (ImageView)convertView.findViewById(R.id.albumCover);

        if(this.albumListResponseObject.getAlbumList().get(position).getAlbumCover() != null) {
            cover.setImageBitmap(this.albumListResponseObject.getAlbumList().get(position).getAlbumCover());
        } else {
            cover.setImageResource(R.drawable.noimage);
        }

        return convertView;
    }
}
