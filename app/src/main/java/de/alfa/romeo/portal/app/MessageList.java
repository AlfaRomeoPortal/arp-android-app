package de.alfa.romeo.portal.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.asyntasks.GetMessageListTask;
import de.alfa.romeo.portal.couchbase.CouchbaseHelper;

/**
 * Created by andre on 12.05.14.
 */
public class MessageList extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private ListView lvMessages;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_messagelist);
        getActionBar().setTitle(R.string.actionbar_messages);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        swipeLayout = (SwipeRefreshLayout)findViewById(R.id.content);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.arp_gray_dark,
                R.color.arp_red_light,
                R.color.arp_gray_light,
                R.color.arp_red_light);

        super.onCreate(savedInstanceState);

        if(!isUserPreferencesSet()) {
            Intent preferences = new Intent(MessageList.this, Preferences.class);
            startActivity(preferences);
        } else {
            swipeLayout.setRefreshing(true);
            this.onRefresh();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.message_list_actions, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onRefresh() {
        // load listview data
        CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
        Map<String, Object> userPreferences = couchbaseHelper.queryUserPreferences();
        couchbaseHelper.closeDb();

        List<String> userLoginInfo = new ArrayList<String>();
        userLoginInfo.add(userPreferences.get("login").toString());
        userLoginInfo.add(userPreferences.get("password").toString());

        new GetMessageListTask(MessageList.this).execute(userLoginInfo);
    }
}
