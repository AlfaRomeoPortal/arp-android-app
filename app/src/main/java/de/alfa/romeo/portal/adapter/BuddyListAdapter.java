package de.alfa.romeo.portal.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.alfa.romeo.portal.app.BaseActivity;
import de.alfa.romeo.portal.app.R;
import de.alfa.romeo.portal.models.buddy.BuddyListResponseObject;
import de.alfa.romeo.portal.models.buddy.BuddyObject;

/**
 * Created by andre on 15.05.14.
 */
public class BuddyListAdapter extends BaseAdapter {

    private BuddyListResponseObject buddyListResponseObject;
    private Activity activity;

    public BuddyListAdapter(BuddyListResponseObject buddyListResponseObject, Activity activity){
        this.buddyListResponseObject = buddyListResponseObject;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return this.buddyListResponseObject.getResponse().getOnlineUsers().size();
    }

    @Override
    public Object getItem(int position) {
        return this.buddyListResponseObject.getResponse().getOnlineUsers().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView==null)
        {
            LayoutInflater inflater =
                    (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.buddy_list_item, parent,false);

            viewHolder = new ViewHolder();
            viewHolder.avatar        = (ImageView)convertView.findViewById(R.id.avatar);
            viewHolder.username       = (TextView)convertView.findViewById(R.id.username);
            viewHolder.online        = (ImageView)convertView.findViewById(R.id.online);
            viewHolder.onlinestatus   = (TextView)convertView.findViewById(R.id.onlinestatus);
            viewHolder.lastonline     = (TextView)convertView.findViewById(R.id.lastonline);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        BuddyObject buddyObject = buddyListResponseObject.getResponse().getOnlineUsers().get(position);

        viewHolder.username.setText(buddyObject.getBuddy().getUsername());
        viewHolder.onlinestatus.setText(buddyObject.getBuddy().getOnline());

        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date sendDate = new Date(buddyObject.getBuddy().getLastActivity()*1000);
        viewHolder.lastonline.setText(dateFormat.format(sendDate).toString());



        if(buddyObject.getBuddy().getBitmap() != null) {
            viewHolder.avatar.setImageBitmap(buddyObject.getBuddy().getBitmap());
        } else {
            viewHolder.avatar.setImageResource(R.drawable.noimage);
        }

        Log.d(BaseActivity.TAG, "called getView " + position);

        return convertView;
    }



    static class ViewHolder {
        ImageView avatar;
        TextView username;
        ImageView online;
        TextView onlinestatus;
        TextView lastonline;
    }
}
