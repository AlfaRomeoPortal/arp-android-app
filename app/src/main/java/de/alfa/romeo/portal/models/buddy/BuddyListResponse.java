package de.alfa.romeo.portal.models.buddy;

import java.util.ArrayList;

/**
 * Created by andre on 16.04.14.
 */
public class BuddyListResponse {
    private ArrayList<BuddyObject> onlineUsers;
    private ArrayList<BuddyObject> offlineUsers;
    private String buddyIds;

    public BuddyListResponse(){}


    public ArrayList<BuddyObject> getOnlineUsers() {
        return onlineUsers;
    }

    public void setOnlineUsers(ArrayList<BuddyObject> onlineUsers) {
        this.onlineUsers = onlineUsers;
    }

    public ArrayList<BuddyObject> getOfflineUsers() {
        return offlineUsers;
    }

    public void setOfflineUsers(ArrayList<BuddyObject> offlineUsers) {
        this.offlineUsers = offlineUsers;
    }

    public String getBuddyIds() {
        return buddyIds;
    }

    public void setBuddyIds(String buddyIds) {
        this.buddyIds = buddyIds;
    }
}
