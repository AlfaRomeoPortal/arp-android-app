package de.alfa.romeo.portal.asyntasks;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.net.URL;

import de.alfa.romeo.portal.app.R;
import de.alfa.romeo.portal.helper.BitmapDownloader;

/**
 * Created by andre on 18.05.14.
 */
public class DownloadImageTask extends AsyncTask<URL, Integer, Bitmap> {

    private ImageView imageView;

    public DownloadImageTask(ImageView imageView){
        this.imageView = imageView;
    }

    @Override
    protected Bitmap doInBackground(URL... params) {
        Bitmap bitmap = BitmapDownloader.loadBitmap(params[0]);
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(bitmap != null && bitmap.getByteCount() > 1) {
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageResource(R.drawable.noimage);
        }
    }
}
