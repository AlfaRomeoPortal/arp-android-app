package de.alfa.romeo.portal.asyntasks;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.adapter.MessageListAdapter;
import de.alfa.romeo.portal.app.MessageListActionsDialog;
import de.alfa.romeo.portal.app.R;
import de.alfa.romeo.portal.app.ShowMessage;
import de.alfa.romeo.portal.communication.ArpConnector;
import de.alfa.romeo.portal.models.LoginResponseObject;
import de.alfa.romeo.portal.models.message.MessageListItem;
import de.alfa.romeo.portal.models.message.MessageListResponseObject;

/**
 * Created by andre on 01.08.14.
 */
public class GetMessageListTask extends AsyncTask<List<String>,Integer, MessageListResponseObject>{

    private Activity activity;

    public GetMessageListTask(Activity activity){
        this.activity = activity;
    }

    @Override
    protected MessageListResponseObject doInBackground(List<String>... params) {

        List<String> userLogin = params[0];

        ArpConnector arpConnector = new ArpConnector();
        MessageListResponseObject messageListResponseObject = null;
        LoginResponseObject respLogin = null;

        // api init
        Map<String,Object> inputParams = arpConnector.apiInit();

        // apii login
        respLogin = arpConnector.login(userLogin.get(0), userLogin.get(1), inputParams);


        if(respLogin != null) {
            return messageListResponseObject = arpConnector.getMessageList(inputParams);
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(MessageListResponseObject messageListResponseObject) {
        ListView lvMessages = (ListView)activity.findViewById(R.id.lvMessages);
        SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout)activity.findViewById(R.id.content);

        Toast toast;
        if(messageListResponseObject != null) {

            if(messageListResponseObject.getMessages().size() == 0){
                toast = Toast.makeText(activity.getApplicationContext(), "Dein Postfach ist leer.", Toast.LENGTH_SHORT);
                toast.show();
                activity.finish();
                return;
            }

            final MessageListAdapter messageListAdapter = new MessageListAdapter(messageListResponseObject, activity);

            lvMessages.setAdapter(messageListAdapter);

            lvMessages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent showMessage = new Intent(activity.getApplicationContext(),ShowMessage.class);
                    showMessage.putExtra("messageId", ((MessageListItem)messageListAdapter.getItem(position)).getPmId());
                    showMessage.putExtra("messageTitle", ((MessageListItem)messageListAdapter.getItem(position)).getTitle());

                    activity.startActivity(showMessage);
                }
            });

            lvMessages.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent actions = new Intent(activity.getApplicationContext(), MessageListActionsDialog.class);
                    actions.putExtra("messageId", ((MessageListItem)messageListAdapter.getItem(position)).getPmId());
                    activity.startActivity(actions);

                    return true;
                }
            });


            toast = Toast.makeText(activity.getApplicationContext(), "Max. 50 deiner letzten PMs wurden geladen.", Toast.LENGTH_SHORT);
            toast.show();

        } else {
            toast = Toast.makeText(activity.getApplicationContext(), "Deine PMs konnten nicht geladen werden.", Toast.LENGTH_SHORT);
            toast.show();
            activity.finish();
        }

        // stop refresh animation
        swipeLayout.setRefreshing(false);
    }
}
