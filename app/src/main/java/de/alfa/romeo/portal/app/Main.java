package de.alfa.romeo.portal.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.asyntasks.GetAlbumListTask;
import de.alfa.romeo.portal.couchbase.CouchbaseHelper;
import de.alfa.romeo.portal.service.MessageService;


/**
 * Created by andre on 13.04.14.
 */
public class Main extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //setContentView(R.layout.activity_main);

        setContentView(R.layout.activity_album_main);

        getActionBar().setTitle(R.string.actionbar_title);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        super.onCreate(savedInstanceState);

        if(!isUserPreferencesSet()) {
            Intent preferences = new Intent(Main.this, Preferences.class);
            startActivity(preferences);
        } else {
//            WebView webView = (WebView) findViewById(R.id.webView);
//            webView.loadUrl("http://www.alfa-romeo-portal.de/arp-app/android.html");
//
//            WebSettings webSettings = webView.getSettings();
//            webSettings.setJavaScriptEnabled(true);
//            webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
//            webSettings.setSupportZoom(true);
//            webSettings.setBuiltInZoomControls(true);
//            webView.setWebViewClient(new MyWebViewClient());

            // login user to welcome him
            CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
            Map<String, Object> userPreferences = couchbaseHelper.queryUserPreferences();
            couchbaseHelper.closeDb();

            List<String> userLoginInfo = new ArrayList<String>();
            userLoginInfo.add(userPreferences.get("login").toString());
            userLoginInfo.add(userPreferences.get("password").toString());

            // start notification service if enabled
            if(Boolean.valueOf(userPreferences.get("enableNotification").toString())){
                Intent service = new Intent(getApplicationContext(), MessageService.class);
                startService(service);
            }


            // load albums and refresh gridview
            new GetAlbumListTask(Main.this).execute(userLoginInfo);

            // TODO: check if vbulletin is active
            // ...

        }
    }

    public class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains("alfa-romeo-portal.de")) {
                view.loadUrl(url);
                return false;
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
                return true;
            }
        }
    }
}
