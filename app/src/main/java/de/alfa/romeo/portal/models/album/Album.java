package de.alfa.romeo.portal.models.album;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;

import de.alfa.romeo.portal.adapter.AlbumListAdapter;
import de.alfa.romeo.portal.app.BaseActivity;
import de.alfa.romeo.portal.helper.BitmapDownloader;

/**
 * Created by andre on 23.08.14.
 */
public class Album {

    private Integer albumId;
    private String title;
    private String description;
    private String albumPictureUrl;
    private String lastPictureDate;
    private Integer numberOfPictures;
    private Bitmap albumCover;

    private AlbumListAdapter albumListAdapter;

    public Album(Integer albumId, String title, String description, String albumPictureUrl, String lastPictureDate, Integer numberOfPictures) {
        this.albumId = albumId;
        this.title = title;
        this.description = description;
        this.albumPictureUrl = albumPictureUrl;
        this.lastPictureDate = lastPictureDate;
        this.numberOfPictures = numberOfPictures;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlbumPictureUrl() {
        return albumPictureUrl;
    }

    public void setAlbumPictureUrl(String albumPictureUrl) {
        this.albumPictureUrl = albumPictureUrl;
    }

    public String getLastPictureDate() {
        return lastPictureDate;
    }

    public void setLastPictureDate(String lastPictureDate) {
        this.lastPictureDate = lastPictureDate;
    }

    public Integer getNumberOfPictures() {
        return numberOfPictures;
    }

    public void setNumberOfPictures(Integer numberOfPictures) {
        this.numberOfPictures = numberOfPictures;
    }

    public AlbumListAdapter getAlbumListAdapter() {
        return albumListAdapter;
    }

    public void setAlbumListAdapter(AlbumListAdapter albumListAdapter) {
        this.albumListAdapter = albumListAdapter;
    }

    public Bitmap getAlbumCover() {
        return albumCover;
    }

    public void setAlbumCover(Bitmap albumCover) {
        this.albumCover = albumCover;
    }

    public void loadAlbumCover(){
        if(albumListAdapter != null){
            String url = this.albumPictureUrl;
            try {
                new ImageDownloadTask().execute(new URL(url));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    // Async Task to fetch buddy avatar image
    private class ImageDownloadTask extends AsyncTask<URL, Integer, Bitmap> {

        @Override
        protected Bitmap doInBackground(URL... params) {

            Log.d(BaseActivity.TAG, "try to load: " + params[0]);

            Bitmap bitmap = BitmapDownloader.loadBitmap(params[0]);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            if(bitmap != null && bitmap.getByteCount() > 1) {
                albumCover = bitmap;

                if(albumListAdapter != null){
                    albumListAdapter.notifyDataSetChanged();
                }
            } else {
            }
        }
    }
}
