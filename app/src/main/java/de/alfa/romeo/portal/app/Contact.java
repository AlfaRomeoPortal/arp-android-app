package de.alfa.romeo.portal.app;

import android.os.Bundle;

/**
 * Created by andre on 17.04.14.
 */
public class Contact extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_contact);
        getActionBar().setTitle(R.string.actionbar_contact);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        super.onCreate(savedInstanceState);
    }

}
