package de.alfa.romeo.portal.models.album;

import java.util.List;

/**
 * Created by andre on 23.08.14.
 */
public class AlbumListResponseObject {

    private List<Album> albumList;
    private Integer numberOfAlbums;
    private Integer actualPage;

    public AlbumListResponseObject() {
    }

    public AlbumListResponseObject(List<Album> albumList, Integer numberOfAlbums, Integer actualPage) {
        this.albumList = albumList;
        this.numberOfAlbums = numberOfAlbums;
        this.actualPage = actualPage;
    }

    public List<Album> getAlbumList() {
        return albumList;
    }

    public void setAlbumList(List<Album> albumList) {
        this.albumList = albumList;
    }

    public Integer getNumberOfAlbums() {
        return numberOfAlbums;
    }

    public void setNumberOfAlbums(Integer numberOfAlbums) {
        this.numberOfAlbums = numberOfAlbums;
    }

    public Integer getActualPage() {
        return actualPage;
    }

    public void setActualPage(Integer actualPage) {
        this.actualPage = actualPage;
    }

    @Override
    public String toString() {
        return "AlbumListResponseObject{" +
                "albumList=" + albumList +
                ", numberOfAlbums=" + numberOfAlbums +
                ", actualPage=" + actualPage +
                '}';
    }
}
