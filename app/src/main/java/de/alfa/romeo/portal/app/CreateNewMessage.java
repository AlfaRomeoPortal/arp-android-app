package de.alfa.romeo.portal.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.asyntasks.SendPmTask;
import de.alfa.romeo.portal.couchbase.CouchbaseHelper;


public class CreateNewMessage extends BaseActivity {

    private int messageId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_create_new_message);
        getActionBar().setTitle(R.string.actionbar_create_message);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        super.onCreate(savedInstanceState);

        if(!isUserPreferencesSet()) {
            Intent preferences = new Intent(CreateNewMessage.this, Preferences.class);
            startActivity(preferences);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_message_actions, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_send : {
                return sendMessage();
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    /**
     * send a new message via api
     */
    private boolean sendMessage(){
        findViewById(R.id.pbSpinner).setVisibility(View.VISIBLE);

        TextView senderName = (TextView)this.findViewById(R.id.sender_name);

        if(senderName.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Bitte trage einen Empfänger ein", Toast.LENGTH_SHORT).show();
            findViewById(R.id.pbSpinner).setVisibility(View.INVISIBLE);
            return false;
        }

        EditText title = (EditText)this.findViewById(R.id.title);

        if(title.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Bitte trage einen Titel ein", Toast.LENGTH_SHORT).show();
            findViewById(R.id.pbSpinner).setVisibility(View.INVISIBLE);
            return false;
        }

        EditText message = (EditText)this.findViewById(R.id.message);

        if(message.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Deine Nachricht hat keinen Inhalt", Toast.LENGTH_SHORT).show();
            findViewById(R.id.pbSpinner).setVisibility(View.INVISIBLE);
            return false;
        }

        // login user to welcome him
        CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
        Map<String, Object> userPreferences = couchbaseHelper.queryUserPreferences();
        couchbaseHelper.closeDb();

        List<String> params = new ArrayList<String>();
        params.add(0, senderName.getText().toString());
        params.add(1, title.getText().toString());
        params.add(2, message.getText().toString());
        params.add(3, String.valueOf(0));
        params.add(4, userPreferences.get("login").toString());
        params.add(5, userPreferences.get("password").toString());

        new SendPmTask(getApplicationContext(), CreateNewMessage.this).execute(params);

        return true;
    }
}
