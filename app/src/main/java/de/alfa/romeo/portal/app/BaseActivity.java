package de.alfa.romeo.portal.app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Map;

import de.alfa.romeo.portal.couchbase.CouchbaseHelper;

/**
 * Created by andre on 20.04.14.
 */
public class BaseActivity extends Activity {

    private String[] listItems;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle drawerToggle;
    private Menu menu;

    public final static String TAG = "ARP-PORTAL-APP";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // actionbar stuff
        //getActionBar().setTitle(R.string.actionbar_title);
        getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#990000")));

        // drawer layout stuff
        listItems = getResources().getStringArray(R.array.sidebar_list);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.sidebar);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        drawerToggle = new ActionBarDrawerToggle((Activity) this, mDrawerLayout, R.drawable.ic_navigation_drawer, 0, 0)
        {
            public void onDrawerClosed(View view)
            {
                getActionBar().setTitle(R.string.actionbar_title);
            }

            public void onDrawerOpened(View drawerView)
            {
                getActionBar().setTitle(R.string.actionbar_main_menu);
            }
        };

        mDrawerLayout.setDrawerListener(drawerToggle);

        mDrawerList = (ListView) findViewById(R.id.sidebar_list);

        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>((Activity)this, R.layout.sidebar_item, listItems));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener(this));
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onResume(){
        super.onResume();
    }


    @Override
    public void onStop(){
        super.onStop();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_new : {
                Intent newMessage = new Intent(this, CreateNewMessage.class);
                startActivity(newMessage);
                break;
            }
//            case R.id.action_contact : {
//                Intent contact = new Intent(this, Contact.class);
//                startActivity(contact);
//                break;
//            }
//            case R.id.action_help : {
//                Intent help = new Intent(this, Help.class);
//                startActivity(help);
//                break;
//            }
            case R.id.action_preferences : {
                Intent help = new Intent(this, Preferences.class);
                startActivity(help);
                break;
            }

            /*
            case R.id.action_logout : {
                // kill couchbase
                CouchbaseHelper cbHelper = new CouchbaseHelper(this);
                cbHelper.deleteDb(CouchbaseHelper.CB_BUCKET_NAME);

                //TODO: delete only api prefenreces document

                // stop message service
                stopService(new Intent(this, MessageService.class));


                Intent main = new Intent(this, Main.class);
                startActivity(main);

                finish();

                break;
            }*/
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                break;
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
        return true;
    }


    public class DrawerItemClickListener implements ListView.OnItemClickListener {

        private Activity activity;

        public DrawerItemClickListener(Activity activity){
            this.activity = activity;
        }


        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);

            switch (position) {
                case (0) : {
                    Intent main = new Intent(this.activity, Main.class);
                    startActivity(main);
                    break;
                }

                case (1) : {
                    Intent buddyList = new Intent(this.activity, BuddyList.class);
                    startActivity(buddyList);
                    break;
                }

                case (2) : {
                    Intent messageList = new Intent(this.activity, MessageList.class);
                    startActivity(messageList);
                    break;
                }

//                case (3) : {
//                    Intent portaloMap = new Intent(this.activity, PortaloMap.class);
//                    startActivity(portaloMap);
//                    finish();
//                    break;
//                }

                case (3) : {
                    Intent alfaSong = new Intent(this.activity, PlayAlfaSong.class);
                    startActivity(alfaSong);
                    break;
                }

//                case (4) : {
//                    Intent arpChat = new Intent(this.activity, ArpChat.class);
//                    startActivity(arpChat);
//                    break;
//                }

            }

        }


        /** Swaps fragments in the main content view */
        private void selectItem(int position) {
            // Highlight the selected item, update the title, and close the drawer
            mDrawerList.setItemChecked(position, true);
            setTitle(listItems[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }


    /**
     * check if user is logged in
     * @return
     */
    public boolean isUserLoggedIn() {
        CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
        Map<String, Object> userPreferences = couchbaseHelper.queryUserPreferences();
        couchbaseHelper.closeDb();

        return (Boolean)userPreferences.get("isLoggedIn");
    }

    /**
     * check if user preferences are set - important for automatic login
     * @return
     */
    public boolean isUserPreferencesSet() {
        CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
        Map<String, Object> userPreferences = couchbaseHelper.queryUserPreferences();
        couchbaseHelper.closeDb();

        return userPreferences != null;
    }



}
