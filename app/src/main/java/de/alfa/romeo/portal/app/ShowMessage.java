package de.alfa.romeo.portal.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.asyntasks.GetMessageTask;
import de.alfa.romeo.portal.couchbase.CouchbaseHelper;

/**
 * Created by andre on 12.05.14.
 */
public class ShowMessage extends BaseActivity {

    private ProgressBar progressBar;
    private int messageId;
    private String messageTitle;
    private TextView message;
    private TextView title;
    private TextView senderName;
    private TextView senderTitle;
    private TextView senderId;
    private ImageView senderImage;
    private ImageView onlineStatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_show_message);
        getActionBar().setTitle(this.messageTitle);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        super.onCreate(savedInstanceState);

        if(!isUserPreferencesSet()) {
            Intent preferences = new Intent(ShowMessage.this, Preferences.class);
            startActivity(preferences);
        } else {
            progressBar     = (ProgressBar)findViewById(R.id.pbSpinner);
            message         = (TextView) findViewById(R.id.messagetext);
            title           = (TextView) findViewById(R.id.messagetitle);
            senderName      = (TextView) findViewById(R.id.sender_name);
            senderTitle     = (TextView) findViewById(R.id.sender_title);
            senderId        = (TextView) findViewById(R.id.sender_id);
            onlineStatus    = (ImageView) findViewById(R.id.onlinestatus);

            // get data from activity_messagelist
            Bundle intentData = getIntent().getExtras();
            if(intentData != null) {
                this.messageId      = intentData.getInt("messageId");
                this.messageTitle   = intentData.getString("messageTitle");
            }

            findViewById(R.id.pbSpinner).setVisibility(View.VISIBLE);


            CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
            Map<String, Object> userPreferences = couchbaseHelper.queryUserPreferences();
            couchbaseHelper.closeDb();

            List<String> parameters = new ArrayList<String>();
            parameters.add(userPreferences.get("login").toString());
            parameters.add(userPreferences.get("password").toString());
            parameters.add(String.valueOf(this.messageId));

            new GetMessageTask(getApplicationContext(), ShowMessage.this).execute(parameters);
        }
    }

    /**
     * start activity for creating a new pm
     */
    private void createMessage() {
        Intent newPM = new Intent(this, CreateMessage.class);
        newPM.putExtra("senderId", senderId.getText());
        newPM.putExtra("senderName", senderName.getText());
        newPM.putExtra("senderTitle", senderTitle.getText());
        newPM.putExtra("title", "AW: " + title.getText());
        newPM.putExtra("onlineStatus", onlineStatus.getVisibility());
        newPM.putExtra("messageId", this.messageId);

        startActivity(newPM);

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.show_message_actions, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_reply : {
                createMessage();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }
}
