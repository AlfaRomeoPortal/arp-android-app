package de.alfa.romeo.portal.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.asyntasks.DownloadImageTask;
import de.alfa.romeo.portal.asyntasks.SendPmTask;
import de.alfa.romeo.portal.couchbase.CouchbaseHelper;


public class CreateMessage extends BaseActivity {

    private int messageId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_create_message);
        getActionBar().setTitle(R.string.actionbar_create_message);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        super.onCreate(savedInstanceState);

        if(!isUserPreferencesSet()) {
            Intent preferences = new Intent(CreateMessage.this, Preferences.class);
            startActivity(preferences);
        } else {
            // get data from activity_messagelist
            Bundle intentData = getIntent().getExtras();
            if(intentData != null) {
                ((TextView)findViewById(R.id.recipient)).setText(intentData.getString("senderId"));
                ((TextView)findViewById(R.id.sender_name)).setText(intentData.getString("senderName"));
                ((TextView)findViewById(R.id.sender_title)).setText(intentData.getString("senderTitle"));

                if(intentData.getInt("onlineStatus") == 0) {
                    //VISIBLE = 0
                    findViewById(R.id.onlinestatus).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.onlinestatus).setVisibility(View.INVISIBLE);
                }

                ((EditText)findViewById(R.id.title)).setText(intentData.getString("title"));

                this.messageId = intentData.getInt("messageId");

                try {
                    String url = "http://www.alfa-romeo-portal.de/arcommunity/image.php?u=" + intentData.getString("senderId");
                    new DownloadImageTask((ImageView)findViewById(R.id.avatar)).execute(new URL(url));
                } catch (MalformedURLException e) {
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_message_actions, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_send : {
                return sendMessage();
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    private boolean sendMessage(){
        findViewById(R.id.pbSpinner).setVisibility(View.VISIBLE);

        TextView senderName = (TextView)this.findViewById(R.id.sender_name);

        if(senderName.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Bitte trage einen Empfänger ein", Toast.LENGTH_SHORT).show();
            findViewById(R.id.pbSpinner).setVisibility(View.INVISIBLE);
            return false;
        }

        EditText title = (EditText)this.findViewById(R.id.title);

        if(title.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Bitte trage einen Titel ein", Toast.LENGTH_SHORT).show();
            findViewById(R.id.pbSpinner).setVisibility(View.INVISIBLE);
            return false;
        }

        EditText message = (EditText)this.findViewById(R.id.message);

        if(message.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Deine Nachricht hat keinen Inhalt", Toast.LENGTH_SHORT).show();
            findViewById(R.id.pbSpinner).setVisibility(View.INVISIBLE);
            return false;
        }

        // login user to welcome him
        CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
        Map<String, Object> userPreferences = couchbaseHelper.queryUserPreferences();
        couchbaseHelper.closeDb();

        List<String> params = new ArrayList<String>();
        params.add(0, senderName.getText().toString());
        params.add(1, title.getText().toString());
        params.add(2, message.getText().toString());
        params.add(3, String.valueOf(this.messageId));
        params.add(4, userPreferences.get("login").toString());
        params.add(5, userPreferences.get("password").toString());

        new SendPmTask(getApplicationContext(), CreateMessage.this).execute(params);

        return true;
    }
}
