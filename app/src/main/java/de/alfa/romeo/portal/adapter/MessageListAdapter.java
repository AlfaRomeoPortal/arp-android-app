package de.alfa.romeo.portal.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.alfa.romeo.portal.app.R;
import de.alfa.romeo.portal.models.message.MessageListItem;
import de.alfa.romeo.portal.models.message.MessageListResponseObject;

/**
 * Created by andre on 15.05.14.
 */
public class MessageListAdapter extends BaseAdapter {

    private MessageListResponseObject messageListResponseObject;
    private Activity activity;

    public MessageListAdapter(MessageListResponseObject messageListResponseObject, Activity activity){
        this.messageListResponseObject = messageListResponseObject;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return this.messageListResponseObject.getMessages().size();
    }

    @Override
    public Object getItem(int position) {
        return this.messageListResponseObject.getMessages().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            LayoutInflater inflater =
                    (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.message_list_item, parent,false);
        }

        ImageView messageIcon       = (ImageView)convertView.findViewById(R.id.mailicon);
        TextView messageTitle       = (TextView)convertView.findViewById(R.id.message);
        TextView messageUsername    = (TextView)convertView.findViewById(R.id.username);
        TextView messageDate        = (TextView)convertView.findViewById(R.id.messagedate);

        MessageListItem message = this.messageListResponseObject.getMessages().get(position);

        if(message.getStatusIcon().equals("old") || message.getStatusIcon().equals("replied")) {
            messageIcon.setImageResource(R.drawable.mailold);
        } else if(message.getStatusIcon().equals("new")) {
            messageTitle.setTypeface(Typeface.DEFAULT);
            messageIcon.setImageResource(R.drawable.mailnew);
            messageTitle.setTypeface(Typeface.DEFAULT_BOLD);
        }

        /*
        // dont need it anymore, used android:ellipsize="end" in template
        String title = message.getTitle();
        if(title.length() > 35) {
            title = title.substring(0,35) + "...";
        }
        */


        messageTitle.setText(message.getTitle());

        messageUsername.setText(message.getSenderUserName());

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date sendDate = new Date((long)message.getSendTime()*1000);
        messageDate.setText(dateFormat.format(sendDate).toString());

        return convertView;
    }

    public String getMessageText(int messageItem){
        return this.messageListResponseObject.getMessages().get(messageItem).getTitle();
    }
}
