package de.alfa.romeo.portal.app;

import android.app.ActivityManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.HashMap;
import java.util.Map;

import de.alfa.romeo.portal.couchbase.CouchbaseHelper;
import de.alfa.romeo.portal.service.MessageService;

/**
 * Created by andre on 26.06.14.
 */
public class Preferences extends BaseActivity implements AdapterView.OnItemSelectedListener {

    private String notificationInterval;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_preferences);
        getActionBar().setTitle(R.string.actionbar_preferences);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        super.onCreate(savedInstanceState);

        CheckBox enableNotification = (CheckBox)findViewById(R.id.cbNotifications);
        Spinner spinner = (Spinner) findViewById(R.id.notificationInterval);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.notification_intervals, R.layout.spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        // Apply click listener
        spinner.setOnItemSelectedListener(this);

        EditText login      = (EditText)findViewById(R.id.txtbLogin);
        EditText password   = (EditText)findViewById(R.id.txtbPassword);


        // preset already made user selections
        CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
        Map<String, Object> userPreferences = couchbaseHelper.queryUserPreferences();
        couchbaseHelper.closeDb();

        if(userPreferences !=null) {
            login.setText(userPreferences.get("login").toString());
            password.setText(userPreferences.get("password").toString());

            if((Boolean)userPreferences.get("enableNotification")){
                //enableNotification.setSelected(true);
                enableNotification.setChecked(true);

                // find the value in the dropdown that has to be preselected
                String notificationInterval = userPreferences.get("notificationInterval").toString();
                ArrayAdapter arrayAdapter = (ArrayAdapter)spinner.getAdapter();
                arrayAdapter.getCount();
                int position = 0;
                for(int i=0;i<arrayAdapter.getCount();i++){
                    String interval = (String)arrayAdapter.getItem(i);
                    boolean contains = interval.contains(" " + notificationInterval + " ");
                    if(contains) {
                        position = i;
                    }
                }

                spinner.setSelection(position);
            }
        }

    }


    /**
     * save given user preferences to couchbase
     */
    public boolean savePreferences() {
        EditText login      = (EditText)findViewById(R.id.txtbLogin);
        EditText password   = (EditText)findViewById(R.id.txtbPassword);
        CheckBox enableNotification = (CheckBox) findViewById(R.id.cbNotifications);
        Spinner spinner     = (Spinner) findViewById(R.id.notificationInterval);

        if(!login.getText().equals("") && !password.getText().equals("")){
            Map<String, Object> preferences = new HashMap<String, Object>();
            preferences.put("document_type", CouchbaseHelper.CB_VIEW_USER_PREFERENCES);
            preferences.put("login", login.getText().toString());
            preferences.put("password", password.getText().toString());

            if(enableNotification.isChecked()){
                preferences.put("enableNotification", true);
            } else {
                preferences.put("enableNotification", false);
            }

            preferences.put("notificationInterval", this.notificationInterval);

            CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
            String documentId = couchbaseHelper.createDocUserPreferences(preferences);
            couchbaseHelper.closeDb();

            // TODO: Service darf hier nicht gestartet werden! Es kann sein, dass der Login falsch ist!
            // start notification service if enabled
            if(enableNotification.isChecked()){
                Intent service = new Intent(getApplicationContext(), MessageService.class);
                startService(service);
            } else {
                // TODO: this is not working!!! service doesnt stop
                // try to stop running service
                if(isMyServiceRunning()){
                    Intent service = new Intent(getApplicationContext(), MessageService.class);
                    stopService(service);
                }
            }
        }


        Intent main = new Intent(Preferences.this, Main.class);
        startActivity(main);
        finish();

        return true;
    }

    public void registerAccount(View view){
        Intent registerAccount = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.alfa-romeo-portal.de/arcommunity/register.php"));
        startActivity(registerAccount);
    }

    public void resetPassword(View view){
        Intent resetPassword = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.alfa-romeo-portal.de/arcommunity/login.php?do=lostpw"));
        startActivity(resetPassword);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.preferences, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i("ARP-PORTAL-APP", String.valueOf(item.getItemId()));

        switch(item.getItemId()){
            case R.id.action_save : {
                return savePreferences();
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String notificationIntervall = (String)parent.getItemAtPosition(position);
        String[] intervall = notificationIntervall.split(" * ");
        this.notificationInterval = intervall[1];
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}



    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("de.alfa.romeo.portal.service.MessageService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
