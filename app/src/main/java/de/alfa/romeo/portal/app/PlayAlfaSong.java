package de.alfa.romeo.portal.app;

import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;


/**
 * Created by andre on 24.06.14.
 */
public class PlayAlfaSong extends BaseActivity {

    private MediaPlayer player;
    private boolean isPlaying = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_play_alfa_song);

        getActionBar().setTitle(R.string.actionbar_play_alfa_song);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        super.onCreate(savedInstanceState);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x - 100;
        int height = size.y;

        ImageView imgBigBounce = (ImageView)findViewById(R.id.imgBigBounce);
        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width,width);
        parms.leftMargin = 10;
        parms.rightMargin = 10;
        parms.bottomMargin = 25;
        parms.gravity = Gravity.CENTER_HORIZONTAL;
        imgBigBounce.setLayoutParams(parms);
    }


    public void playAlfaSong(View button){
        ImageView img = (ImageView)button;

        if(!isPlaying){
            player = MediaPlayer.create(PlayAlfaSong.this, R.raw.ar_song);
            player.start();
            isPlaying = true;

            img.setImageResource(R.drawable.bigbounce2);
        } else {
            player.stop();
            isPlaying = false;
            img.setImageResource(R.drawable.bigbounce1);
        }
    }

}
