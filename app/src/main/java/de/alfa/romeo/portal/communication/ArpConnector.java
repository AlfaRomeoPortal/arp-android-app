package de.alfa.romeo.portal.communication;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

import de.alfa.romeo.portal.app.BaseActivity;
import de.alfa.romeo.portal.encryption.Md5;
import de.alfa.romeo.portal.models.JsonModelMapper;
import de.alfa.romeo.portal.models.LoginResponseObject;
import de.alfa.romeo.portal.models.buddy.BuddyListResponseObject;
import de.alfa.romeo.portal.models.message.MessageListResponseObject;
import de.alfa.romeo.portal.models.message.MessageResponseObject;

/**
 * Created by andre on 08.04.14.
 */
public class ArpConnector {

    private final static String apiURL          = "http://www.alfa-romeo-portal.de/arcommunity/api.php";
    private final static String apiKey          = "mcZ3EKnP"; // "Fbk1DFQK";
    private final static String clientName      = "arpapp";
    private final static String clientVersion   = "0.1";
    private final static String platformName    = "Android";
    private final static String platformVersion = "19";
    private final static String uniqueId        = "FC374U2L7151";
    private final static String apiVersion      = "7";

    private String apiAccessToken = "";
    private String apiSessionhash = "";

    private final static String REQUEST_POST = "POST";
    private final static String REQUEST_GET = "GET";


    /**
     * init api service requests
     * @return
     */
    public Map<String,Object> apiInit(){
        try {
            URL url = new URL(ArpConnector.apiURL +
                    "?api_m=api_init" +
                    "&clientname=" + ArpConnector.clientName +
                    "&clientversion=" + ArpConnector.clientVersion +
                    "&platformname=" + ArpConnector.platformName +
                    "&platformversion=" + ArpConnector.platformVersion +
                    "&uniqueid=" + ArpConnector.uniqueId);

            JSONObject object = this.callUrl(url,ArpConnector.REQUEST_GET,null);
            Map<String,Object> preferences = JsonModelMapper.mapInitResponse(object);

            return preferences;

        } catch (MalformedURLException e) {
            return null;
        }
    }


    /**
     * login user via api
     * @return
     */
    public LoginResponseObject login(String login, String password, Map<String,Object> inputParams){
        try {
            if(login == null || password == null){
                return null;
            }

            String signStr = "api_m=login_login"
                    + "&vb_login_md5password=" + Md5.md5(password)
                    + "&vb_login_username=" + URLEncoder.encode(login, "utf-8");

            // The signature is the md5 value of $signstr + accesstoken + clientid + secret + API key
            String signature = Md5.md5(signStr + inputParams.get("accesstoken")
                    + inputParams.get("apiClientId") + inputParams.get("secret") + apiKey);

            URL url = new URL(ArpConnector.apiURL +
                    "?api_m=login_login" +
                    "&api_v=" + ArpConnector.apiVersion +
                    "&api_c=" + inputParams.get("apiClientId") +
                    "&api_s=" + inputParams.get("accesstoken") +
                    "&api_sig=" + signature +
                    "&vb_login_username=" + URLEncoder.encode(login, "utf-8") +
                    "&vb_login_md5password=" + Md5.md5(password));

            //params.put("api_m", "login_login");
            Map<String,Object> params = new LinkedHashMap<String, Object>();
            params.put("vb_login_username", URLEncoder.encode(login, "utf-8"));
            params.put("vb_login_md5password", Md5.md5(password));

            JSONObject object = this.callUrl(url,ArpConnector.REQUEST_POST, this.generatePostData(params)); //this.generatePostData(params)
            LoginResponseObject loginResponseObject = JsonModelMapper.mapLoginResponse(object);
            return loginResponseObject;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * get list of buddys for current user
     * @return BuddyListResponseObject
     */
    public BuddyListResponseObject getBuddyList(Map<String,Object> inputParams) {

        if(inputParams == null) {
            return null;
        }

        String signStr = "api_m=misc_buddylist";

        // The signature is the md5 value of $signstr + accesstoken + clientid + secret + API key
        String signature = Md5.md5(signStr + inputParams.get("accesstoken")
                + inputParams.get("apiClientId") + inputParams.get("secret") + apiKey);

        try {
            URL url = new URL(ArpConnector.apiURL +
                    "?api_m=misc_buddylist" +
                    "&api_v=" + ArpConnector.apiVersion +
                    "&api_c=" + inputParams.get("apiClientId") +
                    "&api_s=" + inputParams.get("accesstoken") +
                    "&api_sig=" + signature);

            JSONObject object = this.callUrl(url,ArpConnector.REQUEST_GET,null);

            if(checkForErrorMessage(object)){
                BuddyListResponseObject response = JsonModelMapper.mapBuddyListResponse(object);
                return response;
            } else {
                throw new Exception("json response contains error message...");
            }
        } catch (MalformedURLException e) {
            return null;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


    public MessageListResponseObject getMessageList(Map<String,Object> inputParams){
        try{
            String signStr = "api_m=private_messagelist"
                    + "&folderid=" + URLEncoder.encode("0", "utf-8")
                    + "&perpage=" + URLEncoder.encode("50", "utf-8");

            //+ "&sort=" + URLEncoder.encode("date", "utf-8")
            //+ "&order=" + URLEncoder.encode("desc", "utf-8")
            //+ "&pagenumber=" + URLEncoder.encode("1", "utf-8");

            // The signature is the md5 value of $signstr + accesstoken + clientid + secret + API key
            String signature = Md5.md5(signStr + inputParams.get("accesstoken")
                    + inputParams.get("apiClientId") + inputParams.get("secret") + apiKey);

            URL url = new URL(ArpConnector.apiURL +
                    "?api_m=private_messagelist" +
                    "&api_v=" + ArpConnector.apiVersion +
                    "&api_c=" + inputParams.get("apiClientId") +
                    "&api_s=" + inputParams.get("accesstoken") +
                    "&api_sig=" + signature +
                    "&folderid=" + URLEncoder.encode("0", "utf-8") +
                    "&perpage=" + URLEncoder.encode("50", "utf-8"));

            JSONObject object = this.callUrl(url, ArpConnector.REQUEST_GET, null);

            if(checkForErrorMessage(object)){
                MessageListResponseObject response = JsonModelMapper.mapMessageListResponse(object);
                return response;
            } else {
                throw new Exception("json response contains error message...");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }



    public boolean sendPM(String message,
                             String messageId,
                             String recipient,
                             String title,
                             Map<String,Object> inputParams){
        try {
            String signStr = "api_m=private_insertpm"
                    + "&message=" + URLEncoder.encode(message, "utf-8");

            if(messageId != ""){
                signStr +=
                        "&pmid=" + URLEncoder.encode(messageId, "utf-8");
            }
            signStr +=
                    "&recipients=" + URLEncoder.encode(recipient, "utf-8")
                            + "&savecopy=" + URLEncoder.encode("1", "utf-8")
                            + "&title=" + URLEncoder.encode(title, "utf-8");

            // The signature is the md5 value of $signstr + accesstoken + clientid + secret + API key
            String signature = Md5.md5(signStr + inputParams.get("accesstoken")
                    + inputParams.get("apiClientId") + inputParams.get("secret") + apiKey);

            URL url = new URL(ArpConnector.apiURL +
                    "?api_m=private_insertpm" +
                    "&api_v=" + ArpConnector.apiVersion +
                    "&api_c=" + inputParams.get("apiClientId") +
                    "&api_s=" + inputParams.get("accesstoken") +
                    "&api_sig=" + signature +
                    "&message=" + URLEncoder.encode(message, "utf-8") +
                    "&title=" + URLEncoder.encode(title, "utf-8") +
                    "&recipients=" + URLEncoder.encode(recipient, "utf-8") +
                    "&savecopy=" + URLEncoder.encode("1", "utf-8") +
                    "&pmid=" + URLEncoder.encode(messageId, "utf-8"));

            Map<String,Object> params = new LinkedHashMap<String, Object>();
            if(messageId != "") {
                params.put("pmid", URLEncoder.encode(messageId, "utf-8"));
            }
            params.put("message", URLEncoder.encode(message, "utf-8"));
            params.put("title", URLEncoder.encode(title, "utf-8"));
            params.put("recipients", URLEncoder.encode(recipient, "utf-8"));
            params.put("savecopy", URLEncoder.encode("1", "utf-8"));

            JSONObject jsonObject = this.callUrl(url,ArpConnector.REQUEST_POST, this.generatePostData(params));

            if(checkForErrorMessage(jsonObject)){
                return true;
            } else {
                throw new Exception("json response contains error message...");
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public MessageResponseObject getPm(String messageId, Map<String,Object> inputParams){
        try{
            String signStr = "api_m=private_showpm"
                    + "&pmid=" + URLEncoder.encode(messageId, "utf-8");

            // The signature is the md5 value of $signstr + accesstoken + clientid + secret + API key
            String signature = Md5.md5(signStr + inputParams.get("accesstoken")
                    + inputParams.get("apiClientId") + inputParams.get("secret") + apiKey);

            URL url = new URL(ArpConnector.apiURL +
                    "?api_m=private_showpm" +
                    "&api_v=" + ArpConnector.apiVersion +
                    "&api_c=" + inputParams.get("apiClientId") +
                    "&api_s=" + inputParams.get("accesstoken") +
                    "&api_sig=" + signature +
                    "&pmid=" + URLEncoder.encode(messageId, "utf-8"));

            Map<String,Object> params = new LinkedHashMap<String, Object>();
            params.put("pmid", URLEncoder.encode(messageId, "utf-8"));

            JSONObject jsonObject = this.callUrl(url,ArpConnector.REQUEST_POST, this.generatePostData(params));

            if(checkForErrorMessage(jsonObject)){
                return JsonModelMapper.mapMessageResponse(jsonObject);
            } else {
                throw new Exception("json response contains error message...");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


    /**
     * get all available albums
     * @param inputParams
     * @return JSONObject
     */
    public JSONObject getAlbums(Map<String,Object> inputParams) {

        if(inputParams == null) {
            return null;
        }

        String signStr = "api_m=album_overview";

        // The signature is the md5 value of $signstr + accesstoken + clientid + secret + API key
        String signature = Md5.md5(signStr + inputParams.get("accesstoken")
                + inputParams.get("apiClientId") + inputParams.get("secret") + apiKey);

        try {
            URL url = new URL(ArpConnector.apiURL +
                    "?api_m=album_overview" +
                    "&api_v=" + ArpConnector.apiVersion +
                    "&api_c=" + inputParams.get("apiClientId") +
                    "&api_s=" + inputParams.get("accesstoken") +
                    "&api_sig=" + signature);

            JSONObject object = this.callUrl(url,ArpConnector.REQUEST_GET,null);
            return object;
//            if(checkForErrorMessage(object)){
//                BuddyListResponseObject response = JsonModelMapper.mapBuddyListResponse(object);
//                return response;
//            } else {
//                throw new Exception("json response contains error message...");
//            }
        } catch (MalformedURLException e) {
            return null;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * create connection to service and call url
     * @param url
     * @param method
     * @param postDataBytes
     * @return
     */
    private JSONObject callUrl(URL url, String method, byte[] postDataBytes){

        Log.d(BaseActivity.TAG, "URL: " +url.toString());

        // open connection, send post data, parse response to JSON
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection)url.openConnection();

            if(method.equals(ArpConnector.REQUEST_POST)){
                conn.setRequestMethod(ArpConnector.REQUEST_POST);
                if(postDataBytes != null) {
                    conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                }
            } else {
                conn.setRequestMethod(ArpConnector.REQUEST_GET);
            }

            conn.setRequestProperty("User-Agent", "Android");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setDoOutput(true);

            if(method.equals(ArpConnector.REQUEST_POST) && postDataBytes != null) {
                conn.getOutputStream().write(postDataBytes);
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null) {
                builder.append(line);
            }

            try {
                Log.d(BaseActivity.TAG, builder.toString());

                JSONObject object = (JSONObject) new JSONTokener(builder.toString()).nextValue();
                return object;

            } catch (JSONException e) {
                return null;
            }
        } catch (IOException e) {
            return null;
        }
    }


    /**
     * generate byte array containing the POST data
     * @param params
     * @return
     */
    private byte[] generatePostData(Map<String,Object> params){
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(param.getKey());
            postData.append('=');
            postData.append(param.getValue());
        }
        byte[] postDataBytes = postData.toString().getBytes();
        return postDataBytes;
    }


    /**
     * check if json response contains error message
     *
     * @param jsonObject
     * @return
     */
    private boolean checkForErrorMessage(JSONObject jsonObject){
        try {
            if(jsonObject.getJSONObject("response").getJSONArray("errormessage") != null) {
                JSONArray errorMessage = jsonObject.getJSONObject("response").getJSONArray("errormessage");

                // invalid access token - login again
                if(errorMessage.get(0).equals("invalid_accesstoken")){
                    return false;
                } else if(errorMessage.get(0).equals("invalid_api_signature")){
                    return false;
                }
            }
        } catch (JSONException e) {
            // don't handle this exception
            // exception means there is no errormessage
        }
        return true;
    }
}
