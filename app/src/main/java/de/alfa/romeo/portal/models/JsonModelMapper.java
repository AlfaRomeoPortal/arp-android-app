package de.alfa.romeo.portal.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.models.album.Album;
import de.alfa.romeo.portal.models.album.AlbumListResponseObject;
import de.alfa.romeo.portal.models.buddy.Buddy;
import de.alfa.romeo.portal.models.buddy.BuddyListResponse;
import de.alfa.romeo.portal.models.buddy.BuddyListResponseObject;
import de.alfa.romeo.portal.models.buddy.BuddyObject;
import de.alfa.romeo.portal.models.message.MessageListItem;
import de.alfa.romeo.portal.models.message.MessageListResponseObject;
import de.alfa.romeo.portal.models.message.MessageResponseObject;

/**
 * Created by andre on 20.04.14.
 */
public class JsonModelMapper {

    private JsonModelMapper(){
    }


    public static Map<String,Object> mapInitResponse(JSONObject jsonObject){
        Map<String, Object> preferences = new HashMap<String, Object>();

        try {
            preferences.put("accesstoken", jsonObject.getString("apiaccesstoken"));
            preferences.put("sessionHash", jsonObject.getString("sessionhash"));
            preferences.put("secret", jsonObject.getString("secret"));
            preferences.put("apiClientId", jsonObject.getString("apiclientid"));
            preferences.put("bbactive", jsonObject.getString("bbactive"));

            return preferences;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static AlbumListResponseObject mapAlbumListResponse(JSONObject jsonObject){
        AlbumListResponseObject albumListResponseObject = new AlbumListResponseObject();

        try{
            albumListResponseObject.setNumberOfAlbums(
                    Integer.valueOf(jsonObject.getJSONObject("response").getJSONObject("albumcount").getString("total")));

            albumListResponseObject.setActualPage(1);

            JSONArray jsonArray = jsonObject.getJSONObject("response").getJSONArray("latestbits");

            List<Album> albumList = new ArrayList<Album>();
            for(int i=0;i<jsonArray.length();i++){

                JSONObject jsonAlbum = ((JSONObject)jsonArray.get(i)).getJSONObject("album");

                Album album = new Album(Integer.valueOf(jsonAlbum.getString("albumid")),
                        jsonAlbum.getString("title_html"),
                        jsonAlbum.getString("description_html"),
                        jsonAlbum.getString("pictureurl"),
                        jsonAlbum.getString("lastpicturedate"),
                        Integer.valueOf(jsonAlbum.getString("picturecount")));

                albumList.add(album);
            }

            albumListResponseObject.setAlbumList(albumList);


        }catch(JSONException e){
            e.printStackTrace();
        }

        return albumListResponseObject;
    }

    /**
     * maps a login response json object to a login response object
     * @param jsonObject
     * @return LoginResponseObject
     */
    public static LoginResponseObject mapLoginResponse(JSONObject jsonObject){
        LoginResponseObject loginResponseObject = new LoginResponseObject();

        try {
            JSONArray jsonArray = jsonObject.getJSONObject("response").getJSONArray("errormessage");
            loginResponseObject.setUserId(jsonObject.getJSONObject("session").getString("userid"));
            loginResponseObject.setUserName(jsonArray.getString(1));
            loginResponseObject.setDbSessionHash(jsonObject.getJSONObject("session").getString("dbsessionhash"));
            loginResponseObject.setAction(jsonArray.getString(0));

            return loginResponseObject;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * maps a json object to a message response object
     * @param jsonObject
     * @return MessageResponseObject
     */
    public static MessageResponseObject mapMessageResponse(JSONObject jsonObject){
        MessageResponseObject messageResponseObject = new MessageResponseObject();

        if(jsonObject != null) {

            try {
                if (jsonObject.getJSONObject("response") != null) {

                    JSONObject post = jsonObject.getJSONObject("response").getJSONObject("HTML").getJSONObject("postbit").getJSONObject("post");

                    messageResponseObject.setUserId(post.getString("userid"));
                    messageResponseObject.setUserName(post.getString("username"));
                    messageResponseObject.setUserTitle(post.getString("usertitle"));
                    messageResponseObject.setOnlineStatus(post.getJSONObject("onlinestatus").getInt("onlinestatus"));
                    messageResponseObject.setMessage(post.getString("message_plain"));
                    messageResponseObject.setAvatarUrl(post.getString("avatarurl"));
                    messageResponseObject.setTitle(post.getString("title"));

                }
            }catch (Exception e) {}
        }

        return messageResponseObject;
    }


    /**
     * maps a json object to a activity_messagelist response object
     * @param jsonObject
     * @return MessageListResponseObject
     */
    public static MessageListResponseObject mapMessageListResponse(JSONObject jsonObject){
        MessageListResponseObject messageListResponseObject = new MessageListResponseObject();
        List<MessageListItem> messageList = new ArrayList<MessageListItem>();

        // check if there is a response inside
        if(jsonObject != null) {

            try {
                if(jsonObject.getJSONObject("response") != null) {

                    // get info values
                    JSONObject html = jsonObject.getJSONObject("response").getJSONObject("HTML");

                    messageListResponseObject.setTotalPmsInbox(
                            Integer.parseInt(html.get("totalmessages").toString())
                    );

                    messageListResponseObject.setEndMessage(
                            Integer.parseInt(html.get("endmessage").toString())
                    );

                    messageListResponseObject.setStartMessage(
                            Integer.parseInt(html.get("startmessage").toString())
                    );


                    messageListResponseObject.setPmQuota(
                            Integer.parseInt(html.get("pmquota").toString())
                    );

                    messageListResponseObject.setShowPerPage(
                            Integer.parseInt(html.get("perpage").toString())
                    );

                    messageListResponseObject.setTotalPmsStored(
                            Integer.parseInt(html.get("pmtotal").toString())
                    );


                    // get message groups
                    JSONArray messageGroups = new JSONArray();
                    try {
                        messageGroups = html.getJSONArray("messagelist_periodgroups");
                    }catch(Exception e){
                        messageGroups.put(0, html.getJSONObject("messagelist_periodgroups"));
                    }

                    int counter = 1;
                    for(int i=0; i<messageGroups.length();i++){
                        JSONObject group = messageGroups.getJSONObject(i);

                        // get messages from group -> could be an object if there is only one message
                        // or an array if there is more than one message
                        try {
                            JSONArray messages = group.getJSONArray("messagelistbits");

                            for(int m=0; i<messages.length();m++){
                                JSONObject message = messages.getJSONObject(m);

                                if(message != null){
                                    MessageListItem messageItem = new MessageListItem();

                                    messageItem.setId(counter);

                                    messageItem.setTitle( message.getJSONObject("pm").get("title").toString() );
                                    messageItem.setPmId(Integer.parseInt(message.getJSONObject("pm").get("pmid").toString()));
                                    messageItem.setStatusIcon(message.getJSONObject("pm").get("statusicon").toString());
                                    messageItem.setSendTime(Integer.parseInt(message.getJSONObject("pm").get("sendtime").toString()));

                                    messageItem.setSenderUserId(
                                            Integer.parseInt(message.getJSONObject("userbit").getJSONObject("userinfo").get("userid").toString())
                                    );
                                    messageItem.setSenderUserName(message.getJSONObject("userbit").getJSONObject("userinfo").get("username").toString());


                                    messageList.add(messageItem);
                                    counter++;
                                }
                            }

                        } catch(Exception e) {
                            try{
                                JSONObject message = group.getJSONObject("messagelistbits");

                                if(message != null){
                                    MessageListItem messageItem = new MessageListItem();

                                    messageItem.setId(counter);

                                    messageItem.setTitle( message.getJSONObject("pm").get("title").toString() );
                                    messageItem.setPmId(Integer.parseInt(message.getJSONObject("pm").get("pmid").toString()));
                                    messageItem.setStatusIcon(message.getJSONObject("pm").get("statusicon").toString());
                                    messageItem.setSendTime(Integer.parseInt(message.getJSONObject("pm").get("sendtime").toString()));

                                    messageItem.setSenderUserId(
                                            Integer.parseInt(message.getJSONObject("userbit").getJSONObject("userinfo").get("userid").toString())
                                    );
                                    messageItem.setSenderUserName(message.getJSONObject("userbit").getJSONObject("userinfo").get("username").toString());

                                    messageList.add(messageItem);
                                    counter++;
                                }
                            }catch (Exception e1) {}
                        }
                    }
                }
            } catch (Exception e) {}
        }

        messageListResponseObject.setMessages(messageList);
        return messageListResponseObject;
    }

    /**
     * maps a json object to a buddylist response object
     * @param json
     * @return BuddyListResponseObject
     */
    public static BuddyListResponseObject mapBuddyListResponse(JSONObject json) {

        BuddyListResponseObject buddyList = new BuddyListResponseObject();

        // check if there is a response inside
        if(json != null) {

            try {
                if (json.getJSONObject("response") != null) {
                    BuddyListResponse response = new BuddyListResponse();
                    response.setBuddyIds(json.getJSONObject("response").get("buddies").toString());
                    ArrayList<BuddyObject> bs = new ArrayList<BuddyObject>();

                    // check if buddy response is not null and if it is an array or
                    // or object
                    try{
                        JSONArray buddies = json.getJSONObject("response").getJSONArray("onlineusers");
                        for (int i = 0; i < buddies.length(); i++) {
                            bs.add(getBuddyForJsonObject((JSONObject) buddies.get(i)));
                        }
                    } catch(Exception e) {
                        try {
                            JSONObject buddies = json.getJSONObject("response").getJSONObject("onlineusers");
                            bs.add(getBuddyForJsonObject(buddies));
                        } catch(Exception e1) {
                            bs.add(getEmptyBuddy());
                        }
                    }

                    response.setOnlineUsers(bs);
                    buddyList.setResponse(response);
                }

            } catch(Exception e) {}

        }

        return buddyList;
    }



    /*
       ************************************************************
       private helper methods
       ************************************************************
     */


    /**
     * map json buddy to buddy object
     * @param buddy
     * @return BuddyObject
     */
    private static BuddyObject getBuddyForJsonObject(JSONObject buddy){
        BuddyObject bObj = new BuddyObject();
        Buddy b = new Buddy();
        try {
            b.setBuddymark( buddy.getJSONObject("buddy").get("buddymark").toString() );
            b.setInvisible( buddy.getJSONObject("buddy").get("invisible").toString() );
            b.setInvisibleMark( buddy.getJSONObject("buddy").get("invisiblemark").toString() );
            b.setLastActivity( Long.parseLong( buddy.getJSONObject("buddy").get("lastactivity").toString()) );
            b.setOnline( buddy.getJSONObject("buddy").get("online").toString() );
            b.setOnlinestatusphrase( buddy.getJSONObject("buddy").get("onlinestatusphrase").toString() );
            b.setStatusIcon( buddy.getJSONObject("buddy").get("statusicon").toString() );
            b.setUserId(buddy.getJSONObject("buddy").get("userid").toString());
            b.setUsername( buddy.getJSONObject("buddy").get("username").toString() );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        bObj.setBuddy(b);
        return bObj;
    }



    private static BuddyObject getEmptyBuddy(){
        Buddy buddy = new Buddy();
        buddy.setUsername("Zur Zeit ist niemand online");
        buddy.setUserId("-1");
        BuddyObject buddyObject = new BuddyObject();
        buddyObject.setBuddy(buddy);
        return buddyObject;
    }

}
