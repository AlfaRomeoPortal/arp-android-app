package de.alfa.romeo.portal.app;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.asyntasks.GetBuddyListTask;
import de.alfa.romeo.portal.couchbase.CouchbaseHelper;

/**
 * Created by andre on 12.05.14.
 */
public class BuddyList extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, ActionBar.TabListener {

    private ListView lvBuddies;
    private SwipeRefreshLayout swipeLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_buddylist);
        getActionBar().setTitle(R.string.actionbar_buddies);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);



        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        getActionBar().setDisplayShowTitleEnabled(false);

        ActionBar.Tab tab = getActionBar().newTab()
                .setText("Online")
                .setTabListener(this);
        getActionBar().addTab(tab);

        ActionBar.Tab tab2 = getActionBar().newTab()
                .setText("Offline")
                .setTabListener(this);
        getActionBar().addTab(tab2);



        swipeLayout = (SwipeRefreshLayout)findViewById(R.id.content);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.arp_gray_dark,
                R.color.arp_red_light,
                R.color.arp_gray_light,
                R.color.arp_red_light);

        super.onCreate(savedInstanceState);

        if(!isUserPreferencesSet()) {
            Intent preferences = new Intent(BuddyList.this, Preferences.class);
            startActivity(preferences);
        } else {
            swipeLayout.setRefreshing(true);
            this.onRefresh();
        }
    }

    @Override
    public void onRefresh() {
        CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
        Map<String, Object> userPreferences = couchbaseHelper.queryUserPreferences();
        couchbaseHelper.closeDb();

        List<String> userLoginInfo = new ArrayList<String>();
        userLoginInfo.add(userPreferences.get("login").toString());
        userLoginInfo.add(userPreferences.get("password").toString());

        new GetBuddyListTask(getApplicationContext(), BuddyList.this).execute(userLoginInfo);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        if(tab.getText().equals("Online")){

        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}
