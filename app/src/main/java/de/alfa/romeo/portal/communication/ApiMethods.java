package de.alfa.romeo.portal.communication;

/**
 * Created by andre on 08.04.14.
 */
public enum ApiMethods {
    api_init,
    login_login,
    login_logout,
    member,
    misc_buddylist,
    private_messagelist,
    private_showpm,
    private_insertpm
}
