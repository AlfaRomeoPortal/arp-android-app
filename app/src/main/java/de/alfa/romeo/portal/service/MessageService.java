package de.alfa.romeo.portal.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import java.util.Map;

import de.alfa.romeo.portal.asyntasks.ServiceMessagesTask;
import de.alfa.romeo.portal.couchbase.CouchbaseHelper;

/**
 * Created by andre on 31.05.14.
 */
public class MessageService extends Service {

    private int intervall;
    private Handler handler;
    private Thread t;
    private boolean doTask;


    public MessageService(){
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent,flags,startId);

        doTask = true;

        CouchbaseHelper couchbaseHelper = new CouchbaseHelper(getApplicationContext());
        Map<String, Object> userPreferences = couchbaseHelper.queryUserPreferences();
        couchbaseHelper.closeDb();

        intervall = Integer.valueOf(userPreferences.get("notificationInterval").toString())*60*1000;

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                // async task for calling api otherwise we get an exeception here
                new ServiceMessagesTask().execute(MessageService.this);
            }
        };

        t = new Thread(new Runnable(){
            public void run() {
                while(doTask)
                {
                    try {
                        Thread.sleep(intervall); // repeat after given intervall
                        handler.sendEmptyMessage(0);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });

        t.start();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        doTask = false;
        handler.removeCallbacksAndMessages(null);
        t = null;
    }
}
