package de.alfa.romeo.portal.app;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;


public class SplashScreen extends Activity {

    // Splash screen timer
    private final static int SPLASH_TIME_OUT = 1000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_splashscreen);
        super.onCreate(savedInstanceState);

        getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#990000")));
        TextView buildVersion = (TextView) findViewById(R.id.buildversion);

        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            buildVersion.setText("Version: " + pinfo.versionName + " - Build: " + pinfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent main = new Intent(SplashScreen.this, Main.class);
                startActivity(main);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


}
