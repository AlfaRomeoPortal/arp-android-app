package de.alfa.romeo.portal.models.buddy;

/**
 * Created by andre on 16.04.14.
 */
public class BuddyListResponseObject {
    private BuddyListResponse response;

    public BuddyListResponseObject(){}


    public BuddyListResponse getResponse() {
        return response;
    }

    public void setResponse(BuddyListResponse response) {
        this.response = response;
    }
}
