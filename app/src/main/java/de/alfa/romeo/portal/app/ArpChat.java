package de.alfa.romeo.portal.app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pubnub.api.*;

import org.json.*;


/**
 * Created by andre on 13.04.14.
 */
public class ArpChat extends BaseActivity {

    private Pubnub pubnub;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //setContentView(R.layout.activity_main);

        setContentView(R.layout.activity_arp_chat);

        getActionBar().setTitle(R.string.actionbar_title);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        super.onCreate(savedInstanceState);

        if (!isUserPreferencesSet()) {
            Intent preferences = new Intent(ArpChat.this, Preferences.class);
            startActivity(preferences);
        } else {

            // Pubnub Test

            this.pubnub = new Pubnub("demo","demo");

            try {
                pubnub.subscribe("demo", new Callback() {
                    @Override
                    public void successCallback(String channel, Object message) {
                        Log.d(BaseActivity.TAG, "successfully subscribed to pubnub...");
                        Log.d(BaseActivity.TAG, channel + " - " + message.toString());
                    }

                    @Override
                    public void errorCallback(String channel, PubnubError pubnubError) {
                        Log.e(BaseActivity.TAG, "error connecting to pubnub...");
                        Log.e(BaseActivity.TAG, channel + " - " + pubnubError.getErrorString());
                    }

                    @Override
                    public void connectCallback(String channel, Object message) {
                        Log.d(BaseActivity.TAG, "connecting to pubnub...");
                        Log.d(BaseActivity.TAG, channel + " - " + message.toString());
                    }

                    @Override
                    public void reconnectCallback(String channel, Object message) {
                        Log.d(BaseActivity.TAG, "reconnecting to pubnub...");
                        Log.d(BaseActivity.TAG, channel + " - " + message.toString());
                    }

                    @Override
                    public void disconnectCallback(String channel, Object message) {
                        Log.d(BaseActivity.TAG, "disconnecting to pubnub...");
                        Log.d(BaseActivity.TAG, channel + " - " + message.toString());
                    }
                });
            } catch (PubnubException e) {
                Log.e(BaseActivity.TAG, e.getPubnubError().getErrorString());
            }
        }
    }

    public void publishMessage(View button){
        Button publish = (Button)button;

        EditText message = (EditText)this.findViewById(R.id.chatMessage);

        this.pubnub.publish("demo", message.getText().toString(), new Callback() {
            @Override
            public void errorCallback(String channel, PubnubError pubnubError) {
                Log.e(BaseActivity.TAG, "error publishing to pubnub...");
                Log.e(BaseActivity.TAG, channel + " - " + pubnubError.getErrorString());
            }

            @Override
            public void successCallback(String channel, Object message) {
                Log.d(BaseActivity.TAG, "published to pubnub...");
                Log.d(BaseActivity.TAG, channel + " - " + message.toString());
            }
        });

    }
}
