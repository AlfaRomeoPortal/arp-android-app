package de.alfa.romeo.portal.asyntasks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.app.MessageList;
import de.alfa.romeo.portal.communication.ArpConnector;
import de.alfa.romeo.portal.models.LoginResponseObject;

/**
 * Created by andre on 01.08.14.
 */
public class SendPmTask extends AsyncTask<List<String>,Integer, Boolean>{

    private Context context;
    private Activity activity;

    public SendPmTask(Context context, Activity activity){
        this.context = context;
        this.activity = activity;
    }

    @Override
    protected Boolean doInBackground(List<String>... params) {
        List<String> parameter = params[0];

        ArpConnector arpConnector = new ArpConnector();
        LoginResponseObject respLogin = null;

        // api init
        Map<String,Object> inputParams = arpConnector.apiInit();

        // api login
        respLogin = arpConnector.login(parameter.get(4), parameter.get(5), inputParams);

        if(respLogin != null) {
           return arpConnector.sendPM(parameter.get(2).toString(),
                                                        parameter.get(3).toString(),
                                                        parameter.get(0).toString(),
                                                        parameter.get(1).toString(),
                                                        inputParams);
        } else {
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(success) {
            Toast toast = Toast.makeText(this.context, "Deine Nachricht wurde erfolgreich versendet.", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            Toast toast = Toast.makeText(this.context, "Es gab Probleme beim Versand der Nachricht. ", Toast.LENGTH_LONG);
            toast.show();
        }

        Intent messageList = new Intent(activity, MessageList.class);
        activity.startActivity(messageList);
        activity.finish();
    }
}
