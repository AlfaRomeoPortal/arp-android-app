package de.alfa.romeo.portal.asyntasks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.app.Preferences;
import de.alfa.romeo.portal.communication.ArpConnector;
import de.alfa.romeo.portal.models.LoginResponseObject;

/**
 * Created by andre on 01.08.14.
 */
public class LoginUserTask extends AsyncTask<List<String>,Integer, LoginResponseObject>{

    private Context context;
    private Activity activity;

    @Deprecated
    public LoginUserTask(Context context){
        this.context = context;
    }

    public LoginUserTask(Activity activity){
        this.activity = activity;
    }

    @Override
    protected LoginResponseObject doInBackground(List<String>... params) {
        List<String> userLogin = params[0];

        ArpConnector arpConnector = new ArpConnector();
        LoginResponseObject respLogin = null;

        // api init
        Map<String,Object> inputParams = arpConnector.apiInit();

        // api login
        respLogin = arpConnector.login(userLogin.get(0), userLogin.get(1), inputParams);
        return respLogin;
    }

    @Override
    protected void onPostExecute(LoginResponseObject responseObject) {
        if(responseObject != null) {
            if (!responseObject.getUserId().equals("") && !responseObject.getDbSessionHash().equals("") &&
                    responseObject.getAction().equals("redirect_login")) {
                Toast toast = Toast.makeText(this.activity.getApplicationContext(), "Willkommen " + responseObject.getUserName(), Toast.LENGTH_SHORT);
                toast.show();
            } else {
                Toast toast = Toast.makeText(this.activity.getApplicationContext(),
                        "Es gab Probleme mit deinem Login, bitte überprüfe deine Eingaben.\n" +  responseObject.getAction(), Toast.LENGTH_LONG);
                toast.show();

                Intent preferences = new Intent(this.activity.getApplicationContext(), Preferences.class);
                this.activity.startActivity(preferences);
                this.activity.finish();
            }
        }

    }
}
