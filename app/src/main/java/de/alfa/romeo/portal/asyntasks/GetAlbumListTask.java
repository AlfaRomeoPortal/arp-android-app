package de.alfa.romeo.portal.asyntasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import de.alfa.romeo.portal.adapter.AlbumListAdapter;
import de.alfa.romeo.portal.app.BaseActivity;
import de.alfa.romeo.portal.app.R;
import de.alfa.romeo.portal.communication.ArpConnector;
import de.alfa.romeo.portal.models.JsonModelMapper;
import de.alfa.romeo.portal.models.LoginResponseObject;
import de.alfa.romeo.portal.models.album.Album;
import de.alfa.romeo.portal.models.album.AlbumListResponseObject;

/**
 * Created by andre on 01.08.14.
 */
public class GetAlbumListTask extends AsyncTask<List<String>, Integer, AlbumListResponseObject> {

    private Activity activity;

    public GetAlbumListTask(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected AlbumListResponseObject doInBackground(List<String>... params) {

        ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.pbSpinner);
        progressBar.setVisibility(View.VISIBLE);

        List<String> userLogin = params[0];

        ArpConnector arpConnector = new ArpConnector();
        LoginResponseObject respLogin = null;

        // api init
        Map<String, Object> inputParams = arpConnector.apiInit();

        // apii login
        respLogin = arpConnector.login(userLogin.get(0), userLogin.get(1), inputParams);


        if (respLogin != null) {
            JSONObject jsonObject = arpConnector.getAlbums(inputParams);
            if (jsonObject == null) {
                return null;
            } else {
                return JsonModelMapper.mapAlbumListResponse(jsonObject);
            }
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(AlbumListResponseObject albumListResponseObject) {

        Toast toast;
        if (albumListResponseObject != null && albumListResponseObject.getAlbumList() != null) {
            Log.d(BaseActivity.TAG, "Found : " + albumListResponseObject.getAlbumList().size() + " albums...");

            GridView albumGrid = (GridView) this.activity.findViewById(R.id.albumoverview);
            final AlbumListAdapter albumOverviewAdapter = new AlbumListAdapter(this.activity, albumListResponseObject);
            albumGrid.setAdapter(albumOverviewAdapter);

            // set adapter for notification and load async album cover
            for (Album album : albumListResponseObject.getAlbumList()) {
                album.setAlbumListAdapter(albumOverviewAdapter);
                album.loadAlbumCover();
            }

            ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.pbSpinner);
            progressBar.setVisibility(View.GONE);
        }
    }
}
