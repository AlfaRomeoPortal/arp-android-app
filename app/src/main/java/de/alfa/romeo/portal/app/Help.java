package de.alfa.romeo.portal.app;

import android.os.Bundle;

/**
 * Created by andre on 20.04.14.
 */
public class Help extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_help);
        getActionBar().setTitle(R.string.actionbar_help);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        super.onCreate(savedInstanceState);
    }

}
